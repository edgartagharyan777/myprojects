import { WebPageUrl } from '../../component/url'
import { PageTitle } from '../../support/pages/navigation'

describe('Search Car tests', () => {

    before('Allows user navigate to webPage', () => {
        cy.visitWebPage(WebPageUrl.url);
    });

    it('Navigated to UltimateQA page', () => {
        cy.title().should('eq', PageTitle.title);
    })
});