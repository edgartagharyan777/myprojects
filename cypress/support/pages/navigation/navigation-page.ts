import {NavigationElements} from '../navigation/elements'

export class Navigation {

    public clickOnCources() {
        cy.get(NavigationElements.courses).click();
    };

    public clickOnSeleniumCSharp() {
        cy.get(NavigationElements.selenium_c).click();
    };
    
    public clickOnSeleniumResources() {
        cy.get(NavigationElements.selenium_resources).click();
    };

    public clickOnAutomationExcersise() {
        cy.get(NavigationElements.automation_excersises).click();
    };

    public clickOnProServices() {
        cy.get(NavigationElements.pro_service).click();
    };

    public clickOnBlog() {
        cy.get(NavigationElements.blog).click();
    };
};