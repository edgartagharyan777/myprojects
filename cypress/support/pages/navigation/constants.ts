export enum PageTitle {
    title = 'Top 57 Resources to Learn Selenium Webdriver [2020] - Ultimate QA'
}
export enum NavigationTexts {
    courses = 'Courses',
    selenium_java = 'Selenium Java',
    selenium_c = 'Selenium C#',
    selenium_resources = 'Selenium Resources',
    automation_excersises = 'Automation Exercises',
    pro_service = 'Pro Services',
    blog = 'Blog'
}