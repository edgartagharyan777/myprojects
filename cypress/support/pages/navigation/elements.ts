export enum NavigationElements {
    courses = '#menu-item-216155',
    selenium_java = '#menu-item-216153',
    selenium_c = '#menu-item-216154',
    selenium_resources = '#menu-item-6838',
    automation_excersises = '#menu-item-587',
    pro_service = '#menu-item-216868',
    blog = '#menu-item-214031'
}