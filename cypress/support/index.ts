export{};

declare global {
    namespace Cypress {
      interface Chainable {
        visitWebPage(visitWebPage: string): Chainable<string>;
      }
    }
  }